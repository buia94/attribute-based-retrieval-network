import tensorflow as tf

from tensorflow.python.keras.models import Sequential
from tensorflow.python.keras.layers import Dense, Flatten, Conv2D, MaxPooling2D
from tensorflow.python.keras.models import Model
from tensorflow.python.keras.applications.vgg19 import VGG19

def img_net_structure(in_shape, out_shape):
    base_model = VGG19(weights='imagenet', include_top=False, input_shape=in_shape)

    output = base_model.output
    output = Flatten()(output)
    output = Dense(4096,activation='relu')(output)
    output = Dense(out_shape, activation='linear')(output)

    new_model = Model(inputs=base_model.input, outputs=output)

    return new_model

if __name__ == '__main__':
    input_shape = (150,150,3)
    output_shape = 64
    img_net = img_net_structure(input_shape, output_shape)
    img_net.summary() 