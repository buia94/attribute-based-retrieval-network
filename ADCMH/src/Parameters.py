##### PATH PARAMETERS #####
METRICS_FILE_PATH = "data/metrics_file.txt"
IMAGES_PATH = 'data/outputs/'
ATTRIBUTES_PATH = 'data/train_label.xml'
###########################

##### LOAD DATA PARAMETERS #####
CAR_COLORS = ["yellow", "orange", "green", "gray", "red", "blue", "white", "golden", "brown", "black", "purple", "pink"]
CAR_TYPES = ["sedan", "suv", "van", "hatchback", "mpv", "pickup", "bus", "truck", "estate", "sportscar", "RV"]
###############################

##### ADMCH PARAMETERS #####
# data parameters #
TRAINING_SIZE = 10
QUERY_SIZE = 2
DATABASE_SIZE = 12
IMG_SIZE = 150
NUM_OF_ATTRIBUTES = 23
NUM_OF_NEIGHBORS = 2
# hyper-parameters #
BATCH_SIZE = 3
NUM_OF_EPOCHS = 1
ALFA = 1
BETA = 1
MARGIN = 1
CODE_LENGTH = 64
############################