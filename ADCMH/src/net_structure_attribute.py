import tensorflow as tf
from tensorflow.python.keras.models import Sequential
from tensorflow.python.keras.layers import Dense, Flatten, Conv2D, MaxPooling2D
from tensorflow.keras.initializers import RandomNormal, Zeros


def attribute_net_structure(in_shape, out_shape):
  kernel_init = RandomNormal(mean=0.0, stddev=0.01)
  bias_init= Zeros()

  model = Sequential()
  model.add(Dense(4096, activation='relu', input_dim=in_shape, kernel_initializer=kernel_init, bias_initializer=bias_init))
  model.add(Dense(4096, activation='relu', kernel_initializer=kernel_init, bias_initializer=bias_init))
  model.add(Dense(out_shape, activation='linear', kernel_initializer=kernel_init, bias_initializer=bias_init))

  return model

if __name__ == '__main__':
  input_shape = 23
  output_shape = 64
  attr_net = attribute_net_structure(input_shape, output_shape)
  print(attr_net.get_weights())