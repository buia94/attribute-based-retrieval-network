import cv2
from Parameters import CAR_COLORS, CAR_TYPES

def print_img_and_attributes(img, binary_attr):
    attributes = CAR_COLORS + CAR_TYPES

    img_description = ""

    for i in range(binary_attr.shape[0]):
        if(binary_attr[i] == 1) :
             img_description = img_description + " " + attributes[i]

    cv2.imshow(img_description, img )
    cv2.waitKey() 
    