import os
import numpy as np
import tensorflow as tf
import pickle
import math
import matplotlib.pyplot as plt

from tensorflow_addons.metrics import hamming_distance
from tensorflow.keras.preprocessing.image import ImageDataGenerator
from tensorflow.keras import backend as kbe 
from tensorflow.keras.optimizers import SGD, Adam 
from load_data import get_dataset
from net_structure_img import img_net_structure
from net_structure_attribute import attribute_net_structure
from metrics import compute_map, compute_average_NDCG
from Parameters import *
from utility import print_img_and_attributes

#TODO: vedere se adam cambia il lr ogni volta
def train_img_net(F, G, C, similarity_matrix, train_img, img_net, img_net_optimizer):

    # Shuffle all structures using the same indexes        
    ind_permutation = np.random.permutation(TRAINING_SIZE)
    
    for iter in range(math.ceil(TRAINING_SIZE / BATCH_SIZE)):
        
        print("-------- START BATCH IMG_NET: ", iter, " --------")
        
        start = iter * BATCH_SIZE
        batch_ind = ind_permutation[start : start + BATCH_SIZE]	
		
        # Extract train batch
        train_batch = train_img[batch_ind, :, :, :]

        sim_batch = similarity_matrix[batch_ind, batch_ind]
        G_batch = G[:, batch_ind]
        C_batch = C[:, batch_ind]
    
        # Open a GradientTape to record the operations run during the forward pass, which enables autodifferentiation
        with tf.GradientTape() as tape:
            # Run the forward pass of the layer
            F_batch = tf.transpose(img_net(train_batch))
            
            # Compute the loss value for this minibatch
            loss_fn = custom_loss(G_batch, sim_batch, C_batch)
            loss_value = loss_fn(F_batch, F_batch)

        # Use the gradient tape to automatically retrieve the gradients of the trainable variables with respect to the loss
        grads = tape.gradient(loss_value, img_net.trainable_weights)
        
        print(img_net_optimizer)

        # Run one step of optimizer by updating the value of the variables to minimize the loss
        img_net_optimizer.apply_gradients(zip(grads, img_net.trainable_weights))
    
        F[:, batch_ind] = F_batch

        print("-------- END BATCH IMG_NET: ", iter, " --------")

    return F

def train_attr_net(G, F, C, similarity_matrix, train_attr, attr_net, attr_net_optimizer):
     
    ind_permutation = np.random.permutation(TRAINING_SIZE)
    for iter in range(math.ceil(TRAINING_SIZE / BATCH_SIZE)):
        
        print("-------- START BATCH ATTR_NET: ", iter, " --------")

        start = iter * BATCH_SIZE
        batch_ind = ind_permutation[start : start + BATCH_SIZE]		
		
        # Extract train_batch
        train_batch = train_attr[batch_ind, :]    
        sim_batch = similarity_matrix[batch_ind, batch_ind]
        F_batch = F[:, batch_ind]
        C_batch = C[:, batch_ind]

        # Open a GradientTape to record the operations run during the forward pass, which enables autodifferentiation
        with tf.GradientTape() as tape:

            # Run the forward pass of the layer
            G_batch = tf.transpose(attr_net(train_batch))

            # Compute the loss value for this minibatch
            loss_fn = custom_loss(F_batch, sim_batch, C_batch)
            loss_value = loss_fn(G_batch, G_batch)

        # Use the gradient tape to automatically retrieve the gradients of the trainable variables with respect to the loss
        grads = tape.gradient(loss_value, attr_net.trainable_weights)

        # Run one step of optimizer by updating the value of the variables to minimize the loss
        attr_net_optimizer.apply_gradients(zip(grads, attr_net.trainable_weights))

        G[:, batch_ind] = G_batch

        print("-------- END BATCH ATTR_NET: ", iter, " --------")

    return G


# If the function is called during the img_net training features_matrix is G and predicted_features is F
# If the function is called during the attribute_net training features_matrix is F and predicted_features is G
def custom_loss(features_matrix, similarity_matrix, C):
   
    def loss(expected_features, predicted_features):
        num_of_img = predicted_features.shape[1]
        code_length = predicted_features.shape[0]

        matrix_norm = tf.zeros(shape=(num_of_img, num_of_img), dtype=tf.float32) 

        for i in range(num_of_img):
            for j in range(num_of_img):
                vector_diff = predicted_features[:, i] - features_matrix[:, j]
                norm = tf.norm(vector_diff)
                matrix_norm[i, j] = norm
        
        p_denominator = (1+kbe.exp((matrix_norm - MARGIN)))

        p_numerator = tf.fill(shape=(num_of_img, num_of_img), value=(1+math.exp(-MARGIN))).astype('float32')

        p = tf.divide(p_numerator, p_denominator) # matching probability between F and G
        
        lc = - (tf.multiply(similarity_matrix, kbe.log(p)) + tf.multiply((similarity_matrix-1), kbe.log(1-p))) 

        term1 = tf.reduce_sum(lc)
        term1 = tf.dtypes.cast(term1, dtype='float32')
        
        term2 = (kbe.pow(tf.norm(predicted_features-C, ord='fro', axis=(0,1)),2) + kbe.pow(tf.norm(features_matrix-C, ord='fro', axis=(0,1)),2))

        one_matrix = tf.fill(shape=(num_of_img, code_length), value=1.0).astype('float32')
        term3 = (kbe.pow(tf.norm(tf.matmul(predicted_features, one_matrix), ord='fro', axis=(0,1)),2) + kbe.pow(tf.norm(tf.matmul(features_matrix, one_matrix), ord='fro', axis=(0,1)),2))

        total_loss = term1 + ALFA*term2 + BETA*term3

        return total_loss
    
    return loss


def calc_cross_similarity(train_attr_1, train_attr_2):

    Sim = (np.dot(train_attr_1, train_attr_2.transpose()) > 0).astype('float32')
    
    return Sim


def normalize_images(images):

    # create generator that centers pixel values
    image_datagen = ImageDataGenerator(featurewise_center=True)
    
    # calculate the mean on the images training dataset
    image_datagen.fit(images)
    mean_pixel = image_datagen.mean

    normalized_img = images - mean_pixel

    return normalized_img


def split_data(images, attributes):

    # Shuffle the dataset. mantaining the correspondence between image and attribute index
    ind_permutation = np.random.permutation(DATABASE_SIZE)
    images = images[ind_permutation, :, :, :]
    attributes = attributes[ind_permutation, :]

    query_img = images[0:QUERY_SIZE, :, :, :]
    train_img = images[QUERY_SIZE:TRAINING_SIZE + QUERY_SIZE, :, :, :]
    retrieval_img = images[QUERY_SIZE:DATABASE_SIZE, :, :, :]

    query_attr = attributes[0:QUERY_SIZE, :]
    train_attr = attributes[QUERY_SIZE:TRAINING_SIZE + QUERY_SIZE, :]
    retrieval_attr = attributes[QUERY_SIZE:DATABASE_SIZE, :]

    return query_img, train_img, retrieval_img, query_attr, train_attr, retrieval_attr


def generate_retrieval_image_code(retrieval_img, retrieval_attr, img_net, attr_net):

    F = tf.transpose(img_net.predict(retrieval_img, batch_size=BATCH_SIZE))
    G = tf.transpose(attr_net.predict(retrieval_attr, batch_size=BATCH_SIZE))
    C = np.sign(ALFA * (F + G))

    return C

def generate_query_attr_code(query_attr, attr_net):
    
    G = tf.transpose(attr_net.predict(query_attr, batch_size=BATCH_SIZE))
    C = np.sign(G)

    return C
        

def plot_loss_values(loss_values):
    x = list(range(NUM_OF_EPOCHS))
    plt.plot(x, loss_values)
    plt.show()


def print_metrics_to_file(mean_average_precision, average_NDCG):
    metrics_file = open(METRICS_FILE_PATH, 'w') 

    print("TRAINING INFORMATION: ", file = metrics_file)
    print("Epochs: ", NUM_OF_EPOCHS, file = metrics_file)
    print("Database size: ", DATABASE_SIZE, file = metrics_file)
    print("Training size: ", TRAINING_SIZE, file = metrics_file)
    print("Query size: ", QUERY_SIZE, file = metrics_file)
    print("\nMETRICS VALUES:", file = metrics_file)
    print("Mean average precision: ", mean_average_precision, file = metrics_file)
    print("Average NDCG: ", average_NDCG, file = metrics_file)
    print("Loss values per epoch: ", loss_values, file = metrics_file)

    metrics_file.close() 


if __name__ == '__main__':
    os.environ["CUDA_VISIBLE_DEVICES"] = "-1"

    print("Tensorflow version: " , tf.__version__)

    #Loading, normalize and splitting data
    images, attributes = get_dataset(DATABASE_SIZE, IMG_SIZE)
    
    normalized_img = normalize_images(images)

    queries_img, train_img, retrieval_img, queries_attr, train_attr, retrieval_attr = split_data(images, attributes)

    print("--- loading, normalization and splitting data finish ---")
    
    # Get neural network models
    input_shape = IMG_SIZE, IMG_SIZE, 3
    img_net = img_net_structure(input_shape, CODE_LENGTH)

    attr_net = attribute_net_structure(NUM_OF_ATTRIBUTES, CODE_LENGTH)

    # Compute cross similarity matrix
    similarity_matrix = calc_cross_similarity(train_attr, train_attr)

    # Optimizer: i use adam with default parameters
    img_net_optimizer = Adam()
    attr_net_optimizer = Adam()

    # Compute features randomly and binary hash code
    F = tf.convert_to_tensor(np.random.randn(CODE_LENGTH, TRAINING_SIZE), np.float32)   
    G = tf.convert_to_tensor(np.random.randn(CODE_LENGTH, TRAINING_SIZE), np.float32)
    C = tf.math.sign(F+G)

    # Result
    loss_values = []

    for epoch in range(NUM_OF_EPOCHS):
        
        print("============ START EPOCH: ", epoch, " ============")
        
        # Update F
        F = train_img_net(F, G, C, similarity_matrix, train_img,train_attr, img_net, img_net_optimizer)
        print("***** UPDATED F *****")

        # Update G
        G = train_attr_net(G, F, C, similarity_matrix, train_attr, attr_net, attr_net_optimizer)
        print("***** UPDATED G *****")

        # Update C
        C = np.sign(ALFA * (F + G))
        print("***** UPDATED C *****")

        # Compute loss
        loss = custom_loss(G, similarity_matrix, C)
        loss_value = loss(F, F).numpy()
        loss_values.append(loss_value)

        print("============ END EPOCH: ", epoch, " ============")
    
    print("\n------- COMPUTE METRICS -------")

    retrievals_img_code = generate_retrieval_image_code(retrieval_img, retrieval_attr, img_net, attr_net)
    queries_attr_code = generate_query_attr_code(queries_attr, attr_net)
    
    mean_average_precision = compute_map(retrievals_img_code, queries_attr_code, retrieval_attr, queries_attr, NUM_OF_NEIGHBORS)
    average_NDCG = compute_average_NDCG(retrievals_img_code, queries_attr_code, retrieval_attr, queries_attr, NUM_OF_NEIGHBORS)
    
    print_metrics_to_file(mean_average_precision, average_NDCG)    
