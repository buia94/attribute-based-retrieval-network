import numpy as np
from tensorflow_addons.metrics import hamming_distance
import tensorflow as tf
from sklearn.metrics import ndcg_score

''' 
    - The attribute code of the query is used to return the codes of the images closest to it according to Hamming.
    - The query attribute is used to calculate metrics (MAP, NDCG) based on the attributes of the closest images returned before.
    - Codes are matrix #codeX#elements.
    - Attributes are matrix #elementsX#attr
'''

def compute_average_NDCG(retrievals_img_code, queries_attr_code, retrievals_attr, queries_attr, num_of_neighbor):
    num_of_query = queries_attr_code.shape[1]
    average_precision_arr = []
    ndcg_arr = []

    for i in range(num_of_query):
        query_attr_code = queries_attr_code[:, i]
        query_attr = queries_attr[i, :]
        
        neighbors_indexes = get_k_neighbors_ind(query_attr_code, retrievals_img_code, num_of_neighbor)
        k_neighbors_attr = retrievals_attr[neighbors_indexes, :]

        ndcg = compute_NDCG(query_attr, k_neighbors_attr)
        ndcg_arr.append(ndcg)

    # usare intervalli di confidenza
    average_NDCG = np.mean(ndcg_arr)

    return average_NDCG


def compute_NDCG(query_attr, k_neighbors_attr):
    k = k_neighbors_attr.shape[0]
    
    ideal_relevance = []
    relevance_score = []

    for i in range(k):

        if np.array_equal(query_attr, k_neighbors_attr[i, :]):
            relevance_score.append(1)
        else:
            relevance_score.append(0)
    
    relevance_score = np.asarray([relevance_score])
    ideal_relevance = -np.sort(-relevance_score)

    ndcg = ndcg_score(ideal_relevance, relevance_score)

    return ndcg
    

def compute_map(retrievals_img_code, queries_attr_code, retrievals_attr, queries_attr, num_of_neighbor):
    
    num_of_query = queries_attr_code.shape[1]
    average_precision_arr = []

    for i in range(num_of_query):
        query_attr_code = queries_attr_code[:, i]
        query_attr = queries_attr[i, :]
        
        neighbors_indexes = get_k_neighbors_ind(query_attr_code, retrievals_img_code, num_of_neighbor)
        k_neighbors_attr = retrievals_attr[neighbors_indexes, :]

        num_of_relevant = get_num_of_relevant(query_attr, retrievals_attr)

        average_precision = compute_average_precision(query_attr, k_neighbors_attr, num_of_relevant)
        
        average_precision_arr.append(average_precision)

    # usare intervalli di confidenza
    mean_average_precision = np.mean(average_precision_arr)

    return mean_average_precision

def compute_average_precision(query_attr, k_neighbors_attr, relevant):
    correct = 0
    recall_sum = 0
    k = k_neighbors_attr.shape[0]

    for i in range(k):

        if np.array_equal(query_attr, k_neighbors_attr[i, :]):
            correct = correct + 1
            recall_sum = recall_sum + correct/(i+1)
        
    if ((relevant == 0) and (recall_sum == 0)) :
        return 0

    average_precision = recall_sum/relevant

    return average_precision
    
def get_k_neighbors_ind(query_attr_code, retrievals_img_code, k):
    
    hamming_distances = []
    num_of_retrieval = retrievals_img_code.shape[1]
    
    for i in range(num_of_retrieval):
        h_d = hamming_distance(query_attr_code, retrievals_img_code[:, i])
        hamming_distances.append(h_d)

    neighbors_indexes = tf.argsort(hamming_distances, axis=0)
    
    k_neighbors_indexes = neighbors_indexes[0:k]

    return k_neighbors_indexes


# A thing is relevant if the code is completaly equal
def get_num_of_relevant(query_attr, retrievals_attr):
    num_of_retrieval = retrievals_attr.shape[0]
    relevant = 0
    
    for i in range(num_of_retrieval):
        if np.array_equal(query_attr, retrievals_attr[i, :]):
            relevant = relevant + 1
    
    return relevant

if __name__=='__main__':
    
    queries_attr_code = np.transpose([
				    [-1,-1, 1, -1]])
	
    retrievals_img_code = np.transpose([
                    [ 1,-1, 1,-1],
				    [-1,-1, -1,-1],
					[-1,-1, 1,-1],
					[ 1, 1,-1,-1],
					[-1, 1,-1,-1],
					[ 1, 1,-1, 1]])
	
    queries_attr = np.array([
						[1, 1, 0, 0]])
	
    retrievals_attr = np.array([
                            [1, 0, 0, 1],
							[1, 1, 0, 0],
							[1, 1, 0, 0],
							[0, 0, 1, 0],
							[1, 0, 0, 0],
							[0, 0, 1, 0]])

    num_of_neighbor = retrievals_img_code.shape[1]

    mean_average_precision = compute_map(retrievals_img_code, queries_attr_code, retrievals_attr, queries_attr, num_of_neighbor)
    print(mean_average_precision)

    average_NDCG = compute_average_NDCG(retrievals_img_code, queries_attr_code, retrievals_attr, queries_attr, num_of_neighbor)
    print(average_NDCG)


