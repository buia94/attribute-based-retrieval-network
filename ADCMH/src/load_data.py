import numpy as np
import os
import cv2
import math
import xml.etree.ElementTree as ET

from Parameters import *


def get_dataset(dataset_size, refactored_img_size):
    dict_dataset = loading_dataset(dataset_size, refactored_img_size)
    nparray_dataset = np.array(list(dict_dataset.values()), dtype=object)

    #I have to use np.stack() because train_attr is a array of array and not a 2d array
    images = np.stack(nparray_dataset[:,0])
    attributes = np.stack(nparray_dataset[:,1])

    return images, attributes


''' 
The function loads the data (images and attributes) in the following format:
    - Dictionary with a key for each image present (the key is the name of the image) and as an value an array consisting of 2 cells:
        - In the first cell an array representing the image with shape (150, 150, 3)
        - In the second cell an array containing the attribute values, which are 0 or 1 if the attribute is present or not
'''
def loading_dataset(dataset_size, refactored_img_size):
    dataset = {}

    cars_attributes = loading_attributes_from_xml(ATTRIBUTES_PATH, dataset_size)
    cars_imgs = loading_images(IMAGES_PATH, refactored_img_size, dataset_size, list(cars_attributes.keys()))

    for img_name in cars_imgs:
        dataset[img_name] = [cars_imgs[img_name], cars_attributes[img_name]]

    return dataset


def loading_images(path, img_size, number_of_imgs = None, imgs_name = None):
    cars_images = {}

    if imgs_name is None:
        imgs_name = os.listdir(path)
    
    for img_name in imgs_name[:number_of_imgs]:
        try:
            img_array = cv2.imread(os.path.join(path,img_name))
            
            if (img_array is None):
                print("The image", img_name, "is not present")
            
            resized_img_array = cv2.resize(img_array, (img_size, img_size))
            cars_images[img_name] = resized_img_array
        except Exception as e:
            print("Error in resize " + img_name)
        pass

    return cars_images


def loading_attributes_from_xml(path, number_of_imgs = None):
    cars_attributes = {}
    tree = ET.parse(path)
    root = tree.getroot()
    for item in list(root.iter('Item'))[:number_of_imgs]:
        item_dict = item.attrib #dictionary containing all car attributes
        img_name = item_dict["imageName"]
        attributes_dict = get_attributes_dict()
        if all (keys in attributes_dict.keys() for keys in (item_dict["color"], item_dict["type"])):
            attributes_dict[item_dict["color"]] = 1
            attributes_dict[item_dict["type"]] = 1
            cars_attributes[img_name] = np.array(list(attributes_dict.values()))
        else:
            raise KeyError("attribute read by xml not present among the possible attributes")

    return cars_attributes


def get_attributes_dict():
    attributes_dict = {}
    
    keys = CAR_COLORS + CAR_TYPES
    
    for key in keys:
        attributes_dict[key] = 0
    
    return attributes_dict


if __name__ == '__main__':
    
    images, attributes = get_dataset()

    print(images.shape)
    print(attributes.shape)

    # print(attributes[3])
    # img_to_print = images[3]
    # print(img_to_print)
    # print(img_to_print.shape)
    # cv2.imshow('image', img_to_print) 
    # cv2.waitKey() 
    
