import numpy as np
from signal import signal, SIGINT
from sys import exit
import random
from skimage import io
from skimage import img_as_ubyte
import argparse
import os
import sys
from Parameters import *
from xml.dom.minidom import Document, parse, parseString, getDOMImplementation
from mlagents.envs.environment import UnityEnvironment


# Used to create the random angles for the images
def ancestral_sampler_1(pi=[0.5, 0.5], mu=[0, 180], sigma=[20, 20], size=1):
    sigma = [20 for i in range(6)]
    pi = [0.16 for i in range(6)]
    sample = []
    z_list = np.random.uniform(size=size)
    low = 0  # low bound of a pi interval
    high = 0  # higg bound of a pi interval

    for index in range(len(pi)):
        if index > 0:
            low += pi[index - 1]
        high += pi[index]
        s = len([z for z in z_list if low <= z < high])
        sample.extend(np.random.normal(loc=mu[index], scale=np.sqrt(sigma[index]), size=s))

    return sample


def Get_Save_images_by_attributes(env, default_brain, train_mode, attribute_list, cam_id, dataset_size, currentDoc, currentItems, output_dir):
    counter_of_desired_image = 0
    counter_of_saved_image = 0
    multiplicative_factor = 3

    # Get random values for the attributes from the gaussians with prefixed mean 
    angle = np.random.permutation(ancestral_sampler_1(pi=[], mu=attribute_list[:6], size=dataset_size * multiplicative_factor))
    temp_intensity_list = np.random.normal(loc=attribute_list[6], scale=np.sqrt(0.4), size=dataset_size * multiplicative_factor)
    temp_light_direction_x_list = np.random.normal(loc=attribute_list[7], scale=np.sqrt(50), size=dataset_size * multiplicative_factor)
    Cam_height_list = np.random.normal(loc=attribute_list[8], scale=2, size=dataset_size * multiplicative_factor)
    Cam_distance_y_list = np.random.normal(loc=attribute_list[9], scale=3, size=dataset_size * multiplicative_factor)
    
    cam_str = "c" + str(cam_id).zfill(3)
    env_info = env.reset(train_mode=train_mode)[default_brain]
    images = []

    while ((counter_of_saved_image < dataset_size) and (counter_of_desired_image < (dataset_size*multiplicative_factor))):

        if (counter_of_desired_image % 100 == 0):
            print("counter of desired image:", counter_of_desired_image, "for camera number:", cam_id)

        done = False

        if angle[counter_of_desired_image] > 360:
            angle[counter_of_desired_image] = angle[counter_of_desired_image] % 360
        while angle[counter_of_desired_image] < 0:
            angle[counter_of_desired_image] = angle[counter_of_desired_image] + 360

        # Get random values for cam distance and scene id
        Cam_distance_x = random.uniform(-5, 5)
        scene_id = random.randint(1, 59)

        # Get car information, produced by Unity. It return the cars always in the same order
        env_info = env.step([[angle[counter_of_desired_image], temp_intensity_list[counter_of_desired_image], temp_light_direction_x_list[counter_of_desired_image], Cam_distance_y_list[counter_of_desired_image], Cam_distance_x, Cam_height_list[counter_of_desired_image], scene_id, train_mode]])[default_brain]
        done = env_info.local_done[0]

        car_id = int(env_info.vector_observations[0][4])
        color_id = int(env_info.vector_observations[0][5])
        type_id = int(env_info.vector_observations[0][6])

        if done:
            env_info = env.reset(train_mode=train_mode)[default_brain]
            continue

        observation_gray = np.array(env_info.visual_observations[1])
        x, y = (observation_gray[0, :, :, 0] > 0).nonzero()

        observation = np.array(env_info.visual_observations[0])

        if observation.shape[3] == 3 and len(y) > 0 and min(y) > 10 and min(x) > 10:
            # Get car image from car information
            ori_img = observation[0, min(x)-10:max(x)+10, min(y)-10:max(y)+10, :]
            
            counter_of_saved_image = counter_of_saved_image + 1
            
            filename = "0" + str(car_id).zfill(4) + "_" + cam_str + "_" + str(counter_of_saved_image) + ".jpg"

            # If the image doesn't exists yet, create the image and save the xml item in the document
            if not(os.path.isfile(output_dir + filename)):
                io.imsave(output_dir + filename, img_as_ubyte(ori_img))
                Item = currentDoc.createElement('Item')
                Item.setAttribute("type", carType[type_id])
                Item.setAttribute("imageName", filename)
                Item.setAttribute("cameraID", cam_str)
                Item.setAttribute("vehicleID", str(car_id).zfill(4))
                Item.setAttribute("sceneID", str(scene_id))
                Item.setAttribute("color", color[color_id])
                Item.setAttribute("orientation", str(round(angle[counter_of_desired_image], 1)))
                Item.setAttribute("lightInt", str(round(temp_intensity_list[counter_of_desired_image], 1)))
                Item.setAttribute("lightDir", str(round(temp_light_direction_x_list[counter_of_desired_image], 1)))
                Item.setAttribute("camHei", str(round(Cam_height_list[counter_of_desired_image], 1)))
                Item.setAttribute("camDis", str(round(Cam_distance_y_list[counter_of_desired_image] + distance_bias, 1)))
                currentItems.appendChild(Item)

        counter_of_desired_image = counter_of_desired_image + 1
    
    if(counter_of_saved_image == dataset_size):
        print("final counter of saved image:", counter_of_saved_image, "for camera number:", cam_id)


def write_document_to_xml(currentDoc):
    # If the xml file already exists, merge the two documents in a new document
    newDoc = Document()
    newItems = newDoc.createElement('Items')
    newDoc.appendChild(newItems)

    if (os.path.isfile(xml_filename)):
        print("Xml file already exist. Merge the two documents")
        oldDocument = parse(os.path.join(xml_filename))
        for elem in oldDocument.getElementsByTagName('Item'):
            newItems.appendChild(elem)

    for elem in currentDoc.getElementsByTagName('Item'):
        newItems.appendChild(elem)

    with open(xml_filename, 'wb') as f:
        f.write(newDoc.toprettyxml(indent='\t', newl="\n", encoding='utf-8'))
    
    f.close()


def main_function():

    if not os.path.isdir(image_directory_path):
        os.mkdir(image_directory_path)

    # Check Python version
    if (sys.version_info[0] < 3):
        raise Exception(
            "ERROR: ML-Agents Toolkit (v0.3 onwards) requires Python 3")

    env = UnityEnvironment(file_name=env_name)

    # Set the default brain to work with
    default_brain = env.brain_names[0]
    brain = env.brains[default_brain]

    print("Begin generation of ", dataset_size, "vechicles shot by", max_cam_id - min_cam_id, "different cameras")

    currentDoc = Document()
    currentItems = currentDoc.createElement('Items')
    currentDoc.appendChild(currentItems)

    # Tell Python to run the handler() function when SIGINT is received. It's a closure function
    def handler(signal_received, frame):
        write_document_to_xml(currentDoc)
        print('SIGINT or CTRL-C detected. Exiting gracefully')
        exit(0)

    # Set the handler for ctrl+c
    signal(SIGINT, handler)

    for cam_id in range(min_cam_id, max_cam_id):
        Get_Save_images_by_attributes(env, default_brain, train_mode, attribute_list, cam_id, dataset_size, currentDoc, currentItems, image_directory_path)

    write_document_to_xml(currentDoc)


if __name__ == '__main__':
    main_function()

    