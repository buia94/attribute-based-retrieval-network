
color = ["yellow", "orange", "green", "gray", "red", "blue", "white", "golden", "brown", "black", "purple", "pink"]
carType = ["sedan", "suv", "van", "hatchback", "mpv", "pickup", "bus", "truck", "estate", "sportscar", "RV"]
env_name = "./Build-linux/VehicleX" 
xml_filename = "./train_label.xml"
image_directory_path = "./outputs/"

train_mode = False 

distance_bias = 12.11

# Number of images to create for each camera. The total number of different vehicles is 1362
dataset_size = 5000

# Range of cam id 
min_cam_id = 1
max_cam_id = 6

# values that I am setting for the gaussians attribute, but which in the paper set using a neural network during the training
attribute_list = [270, 90, 90, 270, 240, 300, 1.2, 45, 7, 1]