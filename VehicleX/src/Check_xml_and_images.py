import xml.etree.ElementTree as ET
import os
from Parameters import *


def get_image_name_from_xml(path):
    image_names = []
    tree = ET.parse(path)
    root = tree.getroot()
    
    for item in list(root.iter('Item')):
        item_dict = item.attrib #dictionary containing all car attributes
        img_name = item_dict["imageName"]
        image_names.append(img_name)
    
    return image_names

def get_image_name_from_directory(path):
    image_names = []

    for root, directories, files in os.walk(path):
        for file in files:
            image_names.append(file)

    return image_names

if __name__ == '__main__':
    xml_image_names = get_image_name_from_xml(xml_filename)
    directory_image_names = get_image_name_from_directory(image_directory_path)

    xor_image_names = set(xml_image_names) ^ set(directory_image_names)

    print("The xor between the xml_image_names and directory_image_names is: ", xor_image_names)
    